package me.shkschneider.dropbearserver2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import me.shkschneider.dropbearserver2.task.Starter;
import me.shkschneider.dropbearserver2.task.Task.Callback;
import me.shkschneider.dropbearserver2.util.L;

public class MainReceiver extends BroadcastReceiver {

	private static final int PAUSE = 30;

	@Override
	public void onReceive(final Context context, Intent intent) {
		L.d(intent.getAction());

		if (LocalPreferences.getBoolean(context, LocalPreferences.PREF_START_BOOT, LocalPreferences.PREF_START_BOOT_DEFAULT) == true) {
			L.d("Recieved boot completion, waiting +" + PAUSE + "s");
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {

				@Override
				public void run() {
					L.d("Starting server after boot");
					new Starter(context, new Callback<Boolean>() {

						@Override
						public void onTaskComplete(int id, Boolean result) {
							L.d("Boot starter result: " + result);
						}
					}, true).execute();
				}
			}, PAUSE * 1000);

		}
	}
}
