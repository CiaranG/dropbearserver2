package me.shkschneider.dropbearserver2.task;

import java.io.File;

import android.content.Context;

import me.shkschneider.dropbearserver2.R;
import me.shkschneider.dropbearserver2.util.ServerUtils;
import me.shkschneider.dropbearserver2.util.ShellUtils;
import me.shkschneider.dropbearserver2.util.Utils;

public class Repairer extends Task {

	public Repairer(Context context, Callback<Boolean> callback) {
		super(Callback.TASK_INSTALL, context, callback, false);

		if (mProgressDialog != null) {
			mProgressDialog.setTitle("Repairer");
		}
	}

	private Boolean copyToAppData(int resId, String path) {
		if (new File(path).exists() == true && ShellUtils.rm(path) == false) {
			return falseWithError(path);
		}
		if (Utils.copyRawFile(mContext, resId, path) == false) {
			return falseWithError(path);
		}
		if (ShellUtils.chmod(path, "755") == false) {
			return falseWithError(path);
		}
		return true;
	}

	private Boolean copyToSystemXbin(int resId, String tmp, String path) {
		if (Utils.copyRawFile(mContext, resId, tmp) == false) {
			return falseWithError(tmp);
		}
		if (ShellUtils.rm(path) == false) {
			// Ignore
		}
		if (ShellUtils.cp(tmp, path) == false) {
			return falseWithError(path);
		}
		if (ShellUtils.chmod(path, "755") == false) {
			return falseWithError(path);
		}
		return true;
	}

	@Override
	protected Boolean doInBackground(Void... params) {

		publishProgress("Dropbear binary");
		copyToAppData(R.raw.dropbear, ServerUtils.getLocalDir(mContext) + "/dropbear");

		publishProgress("Dropbearkey binary");
		copyToAppData(R.raw.dropbearkey, ServerUtils.getLocalDir(mContext) + "/dropbearkey");

		publishProgress("Remount Read-Write");
		if (Utils.remountReadWrite("/system") == false) {
			return falseWithError("/system RW");
		}

		publishProgress("SFTP binary");
		String tmp = ServerUtils.getLocalDir(mContext) + "/tmp";
		if(!copyToSystemXbin(R.raw.sftp_server, tmp, "/system/xbin/sftp-server"))
                        return false;

		publishProgress("SCP binary");
		if(!copyToSystemXbin(R.raw.scp, tmp, "/system/xbin/scp"))
                        return false;

		publishProgress("Remount Read-Only");
		if (Utils.remountReadOnly("/system") == false) {
			return falseWithError("/system RO");
		}

		publishProgress("Banner");
		copyToAppData(R.raw.banner, ServerUtils.getLocalDir(mContext) + "/banner");

		return true;
	}
}

